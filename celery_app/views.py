from django.http import HttpResponse
from django.shortcuts import render
from django_celery_beat.models import PeriodicTask, CrontabSchedule
import time
import json
from . import blog_helper_function as bfh
from . import blog as blog
from django.http import JsonResponse
import ipdb

# Create your views here.
def test(request):
    blog.tracker_last_page()
    # return latest
    # ipdb.set_trace()
    return JsonResponse(bfh.get_last_page_tracker_dict())


def test2(request):
    bfh.check_cached_last_page_tracker_duration()
    return JsonResponse(bfh.get_last_page_tracker_dict())


def schedule_task_api(request):
    schedule, create = CrontabSchedule.objects.get_or_create(
        hour="*", minute="30", day_of_month="*", month_of_year="*"
    )
    task = PeriodicTask.objects.create(
        crontab=schedule,
        name="dynamic_schedule_test" + str(time.time()),
        task="celery_app.task.test_celery",
        args=json.dumps(
            (
                1,
                2,
                3,
                4,
            )
        ),
        kwargs=json.dumps({"name": "rifat", "hobby": "sleeping", "extra": "sdsdds"}),
    )
    return HttpResponse("Done")
