# Generated by Django 3.2.12 on 2022-03-27 06:19

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('celery_app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lastpagetracker',
            name='record_time',
            field=models.DateTimeField(default=datetime.datetime(2022, 3, 27, 12, 19, 52, 285844)),
        ),
    ]
