from pyexpat import model
from django.db import models
from datetime import datetime

# Create your models here.


class LastPageTracker(models.Model):
    banglatech24 = models.IntegerField(null=True, blank=True)
    amrabondhu = models.IntegerField(null=True, blank=True)
    banglatech = models.IntegerField(null=True, blank=True)
    cadetcollegeblog = models.IntegerField(null=True, blank=True)
    choturmatrik = models.IntegerField(null=True, blank=True)
    sachalayatan = models.IntegerField(null=True, blank=True)
    shopnobaz = models.IntegerField(null=True, blank=True)
    techtunes = models.IntegerField(null=True, blank=True)
    record_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.record_time)

    class Meta:
        db_table = "last_page_tracker"
