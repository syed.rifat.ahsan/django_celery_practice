from celery_app.models import LastPageTracker
from django.core.cache import cache
from datetime import datetime

default_last_page_dict = {
    "banglatech24": 283,
    "amrabondhu": 429,
    "banglatech": 56,
    "cadetcollegeblog": 583,
    "choturmatrik": 1702,
    "sachalayatan": 2472,
    "shopnobaz": 32,
    "techtunes": 1735,
}


def load_initial_last_page_tracker_data():
    "This will run whenever the server starts so that we have the last recorded page data"
    try:
        last_page_tracker_dict = fetch_latest_data_from_db()
    except:
        last_page_tracker_dict = default_last_page_dict
    return last_page_tracker_dict


def cache_last_page_tracker_dict(data):
    print("==============>Caching last page tracker dict======>")
    cache.set("last_page_tracker_dict", data, None)


def fetch_latest_data_from_db():
    print(
        "======================Getting latest data from database======================"
    )
    latest_db_dict = LastPageTracker.objects.last().__dict__
    # removing _state so that its json serializable
    del latest_db_dict["_state"]
    # update the cache whenever data is being fetched from database
    cache_last_page_tracker_dict(latest_db_dict)
    return latest_db_dict


def check_cached_last_page_tracker_duration():
    try:
        tracker_dict = get_last_page_tracker_dict()
        last_cached_datetime = tracker_dict["record_time"]
        current_datetime = datetime.now()
        import ipdb

        ipdb.set_trace()
        difference_in_minute = (current_datetime - last_cached_datetime).seconds / 60
        print(f"Tracker data was last cached --> {difference_in_minute} minutes ago")
        return difference_in_minute
    except Exception as e:
        print("Exception in check_cached_last_page_tracker_duration", e)
        # returning default 5 min
        return 1000


def handle_error(name):
    try:
        # if error happens while fetching the last page number return the last updated number from db
        print("===========Handeling Error================")
        diff_min = check_cached_last_page_tracker_duration()
        if diff_min > 20:
            print(
                "Tracker data cache has not been updated in last 20 min. So making db call"
            )
            latest_db_dict = fetch_latest_data_from_db()
        else:
            print("Handeling tracker method error from cached data")
            latest_db_dict = get_last_page_tracker_dict()
        return latest_db_dict[name]
    except Exception as e:
        print("Error Fetching latest page tracker data from db")
        # if error happens in getting data from db return the default value
        return default_last_page_dict[name]


def get_last_page_tracker_dict():
    try:
        # first check cache
        last_page_tracker_dict = cache.get("last_page_tracker_dict")
        if not last_page_tracker_dict:
            # cache is empty so fetch from db
            return fetch_latest_data_from_db()
        return dict(last_page_tracker_dict)
    except Exception as e:
        print(
            "Exception occurred while getting last page tracker data from db or cache"
        )
        return default_last_page_dict
