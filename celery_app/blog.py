import requests
from bs4 import BeautifulSoup
from celery_app.models import LastPageTracker
from django.core.cache import cache
from . import blog_helper_function as bhf


last_page_tracker_dict = bhf.load_initial_last_page_tracker_data()
print(last_page_tracker_dict)


def bn2enNumber(bnum):
    num_dic = {
        "১": "1",
        "২": "2",
        "৩": "3",
        "৪": "4",
        "৫": "5",
        "৬": "6",
        "৭": "7",
        "৮": "8",
        "৯": "9",
        "০": "0",
    }
    enum = ""
    for c in bnum:
        enum += num_dic[c]
    return int(enum)


def search_number_of_page(url, class_name, n=None):
    r = requests.get(url)
    soup = BeautifulSoup(r.content, "html.parser")
    if n is None:
        return soup.find_all(class_=class_name)
    else:
        return soup.find_all(class_=class_name)[n].getText()


def get_page_num(link: str):
    return str(link).split("page=")[1].split('"')[0]


def last_page_banglatech24(last_page=283):
    try:
        lp = search_number_of_page(
            url="https://banglatech24.com/", class_name="total-pages", n=-1
        )
        return int(lp.split("of")[1].strip())
    except:
        print(
            "banglatech24 get exception for searching last page. returning the default value"
        )
        return bhf.handle_error("banglatech24")


def last_page_amrabondhu(last_page=429):
    try:
        lp = search_number_of_page(
            url="https://www.amrabondhu.com/", class_name="pager-last last"
        )
        return int(get_page_num(lp))
    except:
        print(
            "amarbondhu get exception for searching last page. returning the default value"
        )
        return bhf.handle_error("amrabondhu")


def last_page_banglatech(last_page=56):
    try:
        lp = int(
            search_number_of_page(
                url="https://banglatech.info/", class_name="page-numbers", n=-2
            )
        )
        return lp
    except:
        print(
            "banglatech get exception for searching last page. returning the default value"
        )
        return bhf.handle_error("banglatech")


def last_page_cadetcollegeblog(last_page=583):
    try:
        lp = search_number_of_page(
            url="https://cadetcollegeblog.com/", class_name="page-numbers", n=-2
        )
        return bn2enNumber(lp)
    except:
        print(
            "cadetcollegeblog get exception for searching last page. returning the default value"
        )
        return bhf.handle_error("cadetcollegeblog")


def last_page_choturmatrik(last_page=1702):
    try:
        lp = search_number_of_page(
            url="http://www.choturmatrik.com/", class_name="pager-last last"
        )
        lp = int(get_page_num(lp))
        return last_page
    except:
        print(
            "choturmatrik get exception for searching last page. returning the default value"
        )
        return bhf.handle_error("choturmatrik")


def last_page_sachalayatan(last_page=2472):
    try:
        lp = search_number_of_page(
            url="http://www.sachalayatan.com/", class_name="pager-last last"
        )
        lp = int(get_page_num(lp))
        return last_page
    except:
        print(
            "sachalayatan get exception for searching last page. returning the default value"
        )
        return bhf.handle_error("sachalayatan")


def last_page_shopnobaz(last_page=32):
    try:
        lp = int(
            search_number_of_page(
                url="https://shopnobaz.net/", class_name="page-numbers", n=-2
            )
        )
        return lp
    except:
        print(
            "shopnobaz get exception for searching last page. returning the default value"
        )
        return bhf.handle_error("shopnobaz")


def last_page_techtunes(last_page=1735):
    try:
        lp = search_number_of_page(
            url="https://www.techtunes.io/", class_name="page-link", n=-2
        )
        return int(("").join(lp.split(",")))

    except:
        print(
            "techtunes get exception for searching last page. returning the default value"
        )
        return bhf.handle_error("techtunes")


# muktomona has no clue to detect last page. currently 532

# banglanama has only 9  page currently


def tracker_last_page():

    print("Tracker method called===>")

    banglatech24 = last_page_banglatech24()
    amrabondhu = last_page_amrabondhu()
    banglatech = last_page_banglatech()
    cadetcollegeblog = last_page_cadetcollegeblog()
    choturmatrik = last_page_choturmatrik()
    sachalayatan = last_page_sachalayatan()
    shopnobaz = last_page_shopnobaz()
    techtunes = last_page_techtunes()

    last_page_tracker_dict = {
        "banglatech24": banglatech24,
        "amrabondhu": amrabondhu,
        "banglatech": banglatech,
        "cadetcollegeblog": cadetcollegeblog,
        "choturmatrik": choturmatrik,
        "sachalayatan": sachalayatan,
        "shopnobaz": shopnobaz,
        "techtunes": techtunes,
    }
    print("Saving last_page_tracker---->", last_page_tracker_dict)
    new_record = LastPageTracker(**last_page_tracker_dict)
    new_record.save()
    last_page_tracker_dict = new_record.__dict__
    del last_page_tracker_dict["_state"]
    bhf.cache_last_page_tracker_dict(last_page_tracker_dict)
