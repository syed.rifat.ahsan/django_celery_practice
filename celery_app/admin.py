from django.contrib import admin
from celery_app.models import LastPageTracker

# Register your models here.
admin.site.register(LastPageTracker)
