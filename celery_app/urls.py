from django.urls import path
from .views import test,schedule_task_api,test2

urlpatterns = [
    path('test/', test),
    path('test2/', test2),
     path('test/schedule', schedule_task_api),
]
