# import django

# django.setup()
import time
from celery import shared_task

# from celery_app import blog


@shared_task(bind=True)
def track_blog_lag_page(self, *args, **kwargs):
    from . import blog as blog

    print("Running task to load last pages count")
    blog.tracker_last_page()


@shared_task(bind=True)
def test_celery(self, *args, **kwargs):
    print("This is a test task v2-----sleeping for 3 seconds")
    time.sleep(3)
    print("Args------->")
    print([arg for arg in args])
    print("Kwargs------->")
    print(kwargs)
    for i in range(10):
        print(str(i) + "---------->" + "version 2")
    return "Done"


@shared_task(bind=True)
def write_my_name(self, args):
    print("Sleeping for 20 seconds before writing my name")
    time.sleep(20)
    print(args, "\n")
    print("===========Syed Rifat Ahsan=================")
    print("Task Done!")
