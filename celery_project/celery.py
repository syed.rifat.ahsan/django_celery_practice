from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from django.conf import settings
from celery_app import task
from celery.schedules import crontab
from . import settings as d_settings


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "celery_project.settings")
app = Celery("celery_project")
app.config_from_object(settings, namespace="CELERY")

# app.autodiscover_tasks(lambda: d_settings.INSTALLED_APPS)
# Celery Beat Settings

app.autodiscover_tasks()

# Celery beat
app.conf.beat_schedule = {
    "track_blog_last_page": {
        "task": "celery_app.task.track_blog_lag_page",
        "schedule": crontab(minute="0", hour="*/2"),
    },
    # "task_test_every_1_min": {
    #     "task": "celery_app.task.test_celery",
    #     "schedule": 60,
    #     "args": ("Args1", "Args2"),
    #     "kwargs": {
    #         "Firstname": "Syed",
    #         "Lastname": "Rifat",
    #         "Age": 25,
    #         "Phone": 1234567890,
    #     },
    # },
}


@app.task(bind=True)
def debug_task(self):
    print(f"Request :{self.request!r}")
